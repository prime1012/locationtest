package com.app.locationtest.model

import com.google.gson.annotations.SerializedName

class ResponseModel {

    @SerializedName("display_name")
    var displayName: String? = null
}