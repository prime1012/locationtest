package com.app.locationtest.network

import com.app.locationtest.model.ResponseModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {

    @GET("/reverse.php")
    fun getLocationInfo(@Query("format") format: String, @Query("lat") lat: Double, @Query("lon") lon: Double): Observable<ResponseModel>
}
