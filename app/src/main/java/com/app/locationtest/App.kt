package com.app.locationtest

import android.app.Application
import com.app.locationtest.network.APIService
import com.app.megamall.network.RetrofitFactory
import com.github.salomonbrys.kodein.*

class App : Application(), KodeinAware {

    override val kodein by Kodein.lazy {
        bind<APIService>() with provider {
            RetrofitFactory.getFactory(applicationContext.getString(R.string.api_url))
                .create(APIService::class.java)
        }
    }

}